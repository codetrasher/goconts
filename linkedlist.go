package goconts

type node struct {
	data interface{}
	next *node
}

// LinkedList struct holds information about the current item count and the pointer to the head
// node of the linked list.
type LinkedList struct {
	count int
	head  *node
}

// Push pushed a value into the beginning node of the linked list.
func (ll *LinkedList) Push(d interface{}) {
	if ll.head == nil {
		ll.head = &node{data: d}
	} else {
		n := &node{data: d, next: ll.head}
		ll.head = n
	}
	ll.count++
}

// Pop removes the last Node from the linked list and returns it and the updated count of items in
// the linked list. The node in 'head.next' is moved to 'head' until 'head.next' is nil.
func (ll *LinkedList) Pop() (nodeItem *node, newCount int) {

	if ll.count == 0 && ll.head == nil {
		return
	}

	for {
		if n := ll.head; ll.head.next == nil {
			nodeItem = n
			break
		}
		ll.head = ll.head.next
	}
	ll.count--
	newCount = ll.count
	return
}

// Count return the number of nodes in the linked list structure.
func (ll *LinkedList) Count() int {
	return ll.count
}

/*func (ll *LinkedList) Peek() bool {
    return ll.nodeItem
}*/

/*func (ll *LinkedList) next() *node {

}*/

/*func (ll *LinkedList) All() (ret []interface{}) {

}*/
