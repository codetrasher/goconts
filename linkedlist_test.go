package goconts

import (
	"math/rand"
	"testing"
	"time"
)

func TestPush(t *testing.T) {

	ll := &LinkedList{}
	ll.Push(1)
	if ll.head == nil {
		t.Fail()
	}
	if _, ok := ll.head.data.(int); !ok {
		t.Fail()
	}
	if ll.Count() != 1 {
		t.Fail()
	}
}

func BenchmarkPush(b *testing.B) {
	ll := &LinkedList{}

	for i := 0; i < b.N; i++ {
		ll.Push(i)
	}
}

func TestPop(t *testing.T) {
	ll := &LinkedList{}
	ll.Push(1)
	ll.Push(2)

	if ll.Count() != 2 {
		t.Fail()
	}

	n, c := ll.Pop()
	if v, ok := n.data.(int); ok {
		if v != 1 {
			t.Fail()
		}
	}
	if c != 1 {
		t.Fail()
	}
}

func BenchmarkPop(b *testing.B) {
	ll := &LinkedList{}

	rand.Seed(time.Now().UTC().UnixNano())

	for i := 0; i < b.N; i++ {
		ll.Push(rand.Intn(100))
	}

	for i := 0; i < b.N; i++ {
		ll.Pop()
	}
}
